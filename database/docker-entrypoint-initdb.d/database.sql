--
-- Project Database
--

CREATE TABLE "products" (
    "id" serial PRIMARY KEY,
    "name" varchar(255) NOT NULL UNIQUE,
    "price" float NOT NULL,
    "quantity" integer NOT NULL
);

CREATE TABLE "users" (
    "id" serial PRIMARY KEY,
    "email" varchar(255) NOT NULL UNIQUE,
    "password" varchar(50) NOT NULL
);

CREATE TABLE "orders" (
    "id" serial PRIMARY KEY,
    "user_id" integer NOT NULL,
    CONSTRAINT "user_fk" FOREIGN KEY ("user_id") REFERENCES "users" ("id")
);

CREATE TABLE "order_product" (
    "id" serial PRIMARY KEY,
    "order_id" integer NOT NULL,
    "product_id" integer NOT NULL,
    CONSTRAINT "order_fk" FOREIGN KEY ("order_id") REFERENCES "orders" ("id"),
    CONSTRAINT "product_fk" FOREIGN KEY ("product_id") REFERENCES "products" ("id")
);

