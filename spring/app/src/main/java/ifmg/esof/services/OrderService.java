package ifmg.esof.services;

import ifmg.esof.model.Order;
import ifmg.esof.model.User;
import ifmg.esof.repository.OrderRepository;
import ifmg.esof.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class OrderService {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private UserRepository userRepository;

    public Long createOrder(Order order, Long id) {
        User _user = userRepository.getById(id);
        order.setUser(_user);

        Order _order = orderRepository.save(order);

        return _order.getId();
    }

    public void updateOrder(Long id, User user) {

    }

    public void deleteOrder(Long id) {

    }


    public Order getOrder(Long id) {
        return orderRepository.getById(id);
    }
}
