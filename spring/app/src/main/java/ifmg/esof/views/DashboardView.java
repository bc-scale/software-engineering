package ifmg.esof.views;

import java.lang.Long;
import java.util.ArrayList;
import java.util.List;
import com.vaadin.flow.component.button.Button;
import com.vaadin.flow.component.grid.Grid;
import com.vaadin.flow.component.html.H1;
import com.vaadin.flow.component.Text;
import com.vaadin.flow.component.orderedlayout.HorizontalLayout;
import com.vaadin.flow.component.orderedlayout.VerticalLayout;
import com.vaadin.flow.component.textfield.TextField;
import com.vaadin.flow.component.formlayout.FormLayout;
import com.vaadin.flow.component.page.BodySize;
import com.vaadin.flow.component.page.Viewport;
import com.vaadin.flow.component.details.Details;
import com.vaadin.flow.component.details.DetailsVariant;
import com.vaadin.flow.component.dialog.Dialog;
import com.vaadin.flow.router.BeforeEnterEvent;
import com.vaadin.flow.router.BeforeEnterObserver;
import com.vaadin.flow.router.Route;
import ifmg.esof.model.Order;
import ifmg.esof.model.Product;
import ifmg.esof.services.OrderService;
import ifmg.esof.services.ProductService;
import ifmg.esof.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;

@Route("/dashboard")
@Viewport("width=device-width, minimum-scale=1.0, initial-scale=1.0, user-scalable=yes")
@BodySize(height = "100vh", width = "100vw")
public class DashboardView extends VerticalLayout implements BeforeEnterObserver {
        @Autowired
        private OrderService orderService;

        @Autowired
        private UserService userService;

        @Autowired
        private ProductService productService;

        private Long id;
        private Long orderId;
        private String email;
        private String password;

        private List<Product> products;
        private Grid<Product> grid;
        private Details dts;
        private Text totalValueDisplay;

        private Long productId;

        private float total;

        private Dialog DOrder;
        private Dialog DProduct;
        private Dialog DDeleteProduct;
        private Dialog DUpdateProduct;
        private Dialog DCreateOrder;

        @Override
        public void beforeEnter(BeforeEnterEvent event) {
            // TODO: get session for reroute user

            /*
            if(!event.getNavigationTarget().equals(RegisterView.class)) {
                event.forwardTo(RegisterView.class);
            }
            */
        }

        public void createProductModal() {
            FormLayout columnLayout = new FormLayout();
            columnLayout.setSizeFull();

            TextField name = new TextField();
            name.setLabel("Nome");
            name.setPlaceholder("Digite o nome aqui...");

            TextField price = new TextField();
            price.setLabel("Preço");
            price.setPlaceholder("Digite o preço aqui...");

            TextField quantity = new TextField();
            quantity.setLabel("Quantidade");
            quantity.setPlaceholder("Digite a quantidade aqui...");

            Button added = new Button("Registrar", event -> {
                Product product = new Product(name.getValue(), Float.parseFloat(price.getValue()), Integer.parseInt(quantity.getValue()));

                name.clear();
                price.clear();
                quantity.clear();

                products.add(product);

                this.totalValueDisplay.setText(this.totalValue());

                grid.getDataProvider().refreshAll();

                productService.createProduct(product, new Long(1L));

                this.DProduct.close();
            });
            added.getElement().setAttribute("aria-label", "Registrar Produto");

            Button close = new Button("Fechar", event -> {
                this.DProduct.close();
            });
            close.getElement().setAttribute("aria-label", "Fechar");

            columnLayout.add(name);
            columnLayout.add(price);
            columnLayout.add(quantity);
            columnLayout.add(added);
            columnLayout.add(close);

            Dialog product = new Dialog();

            product.setWidth("400px");
            product.add(columnLayout);

            this.DProduct = product;
        }

        public void updateProductModal() {
            FormLayout columnLayout = new FormLayout();
            columnLayout.setSizeFull();

            TextField name = new TextField();
            name.setLabel("Nome");
            name.setPlaceholder("Digite o nome aqui...");

            TextField price = new TextField();
            price.setLabel("Preço");
            price.setPlaceholder("Digite o preço aqui...");

            TextField quantity = new TextField();
            quantity.setLabel("Quantidade");
            quantity.setPlaceholder("Digite a quantidade aqui...");

            Button update = new Button("Atualizar", event -> {
                Product product = new Product(name.getValue(), Float.parseFloat(price.getValue()), Integer.parseInt(quantity.getValue()));
                product.setId(this.productId);

                // TODO: resolver problema de sessão.
                // productService.updateProduct(product);

                this.products.forEach(_product -> {
                    if(product.getId().equals(_product.getId())) {
                        _product.setPrice(product.getPrice());
                        _product.setName(product.getName());
                        _product.setQuantity(product.getQuantity());
                    }
                });

                this.totalValueDisplay.setText(this.totalValue());

                grid.getDataProvider().refreshAll();

                this.DUpdateProduct.close();
            });
            update.getElement().setAttribute("aria-label", "Atualizar Produto");

            Button close = new Button("Fechar", event -> {
                this.DUpdateProduct.close();
            });
            close.getElement().setAttribute("aria-label", "Fechar Modal");

            columnLayout.add(name);
            columnLayout.add(price);
            columnLayout.add(quantity);
            columnLayout.add(update);
            columnLayout.add(close);

            Dialog product = new Dialog();

            product.setWidth("400px");
            product.add(columnLayout);

            this.DUpdateProduct = product;
        }

        public String totalValue() {
            total = 0.0f;

            this.products.forEach(_product -> {
                this.total += _product.getQuantity() * _product.getPrice();
            });

            return "Total: " + total;
        }

        public void createOrderModal() {
            FormLayout columnLayout = new FormLayout();
            columnLayout.setSizeFull();

            Text text = new Text("Novo Pedido criado! Agora registre o seu primeiro produto.");

            TextField name = new TextField();
            name.setLabel("Nome");
            name.setPlaceholder("Digite o nome aqui...");

            TextField price = new TextField();
            price.setLabel("Preço");
            price.setPlaceholder("Digite o preço aqui...");

            TextField quantity = new TextField();
            quantity.setLabel("Quantidade");
            quantity.setPlaceholder("Digite a quantidade aqui...");

            Button added = new Button("Registrar", event -> {
                Product product = new Product(name.getValue(), Float.parseFloat(price.getValue()), Integer.parseInt(quantity.getValue()));
                products.add(product);

                this.totalValueDisplay.setText(this.totalValue());

                grid.getDataProvider().refreshAll();

                productService.createProduct(product, new Long(1L));

                this.DOrder.close();
            });
            added.getElement().setAttribute("aria-label", "Registrar Produto");

            columnLayout.add(text);
            columnLayout.add(name);
            columnLayout.add(price);
            columnLayout.add(quantity);
            columnLayout.add(added);

            Dialog order = new Dialog();

            order.setWidth("400px");
            order.add(columnLayout);

            this.DOrder = order;
        }

        public void createExposeOrderModal() {
            FormLayout columnLayout = new FormLayout();
            columnLayout.setSizeFull();

            columnLayout.add(new Text("Novo Pedido criado com Sucesso!"));
            columnLayout.add(new Button("Fechar", event -> {
                this.DCreateOrder.close();
            }));

            Dialog order = new Dialog();

            order.setWidth("400px");
            order.add(columnLayout);

            this.DCreateOrder = order;
        }

        public void createModals() {
            this.createExposeOrderModal();
            this.createOrderModal();
            this.createProductModal();
            this.updateProductModal();
        }

        public DashboardView() {
            getElement().executeJs("return window.localStorage.getItem($0);", "email")
                .then(String.class, result -> {
                    this.email = result;
                });
            getElement().executeJs("return window.localStorage.getItem($0);", "password")
                .then(String.class, result -> {
                    this.password = result;
                });
            getElement().executeJs("return window.localStorage.getItem($0);", "id")
                .then(Long.class, result -> {
                    this.id = result;
                });
            this.products = new ArrayList<Product>();

            this.createModals();

            HorizontalLayout header = new HorizontalLayout();
            header.getStyle().set("width", "100%");
            header.getStyle().set("height", "100px");
            header.getStyle().set("borderBottom", "1px solid black");
            header.getStyle().set("padding", "0");
            header.getStyle().set("margin", "0");
            header.getStyle().set("display", "flex");
            header.getStyle().set("justifyContent", "space-between");
            header.getStyle().set("alignItems", "center");

            VerticalLayout main = new VerticalLayout();
            main.getStyle().set("width", "100%");
            main.getStyle().set("height", "calc(100vh - 100px)");
            main.getStyle().set("display", "flex");
            main.getStyle().set("flex-direction", "row");
            main.getStyle().set("padding", "0");
            main.getStyle().set("margin", "0");

            HorizontalLayout content = new HorizontalLayout();
            content.getStyle().set("width", "75%");
            content.getStyle().set("height", "200px");
            content.getStyle().set("flex-grow", "1");
            content.getStyle().set("borderRight", "1px solid black");
            content.getStyle().set("height", "calc(100vh - 100px)");
            content.getStyle().set("padding", "10px");
            content.getStyle().set("margin", "0");

            HorizontalLayout settings = new HorizontalLayout();
            settings.getStyle().set("width", "25%");
            settings.getStyle().set("height", "200px");
            settings.getStyle().set("flex-grow", "1");
            settings.getStyle().set("height", "calc(100vh - 100px)");
            settings.getStyle().set("padding", "10px");
            settings.getStyle().set("margin", "0");

            /* header components */

            H1 title = new H1("ESOF II Dashboard");
            HorizontalLayout titleContainer = new HorizontalLayout();
            titleContainer.add(title);

            HorizontalLayout crud = new HorizontalLayout();

            Button newOrder = new Button("Novo Pedido", event -> {
                this.products.clear();
                grid.getDataProvider().refreshAll();
                Long value = new Long(1L);
                Order order = new Order();
                this.orderId = order.getId();
                orderService.createOrder(order, value);
                // this.DOrder.open();
                this.DCreateOrder.open();
            });
            newOrder.getElement().setAttribute("aria-label", "Novo Pedido");

            Button newProduct = new Button("Novo Produto", event -> {
                this.DProduct.open();
            });
            newProduct.getElement().setAttribute("aria-label", "Novo Produto");



            /* list components */

            this.grid = new Grid<Product>();
            grid.setItems(products);
            grid.addColumn(Product::getName).setHeader("Nome");
            grid.addColumn(Product::getPrice).setHeader("Preço");
            grid.addColumn(Product::getQuantity).setHeader("Quantidade");
            grid.addItemClickListener(event -> {
                VerticalLayout _layout = new VerticalLayout();
                _layout.getStyle().set("width", "100%");

                // TODO: resolve crash item after update product
                Product _product = event.getItem();

                this.productId = _product.getId();

                Text _name = new Text("Nome: " + _product.getName() + "\n");

                Text _price = new Text("Preço: " + _product.getPrice() + "\n");

                Text _quantity = new Text("Quantidade: " + _product.getQuantity() + "\n");

                this.total = 0.0f;

                this.products.forEach(product -> {
                    this.total += product.getQuantity() * product.getPrice();
                });

                Text _total = new Text("Total: " + this.total + "\n");

                _layout.add(_name);
                _layout.add(_price);
                _layout.add(_quantity);
                _layout.add(_total);

                this.dts.setContent(_layout);

                FormLayout columnLayout = new FormLayout();
                columnLayout.setSizeFull();

                Button delete = new Button("Deletar", _event -> {
                    productService.deleteProduct(_product.getId());

                    this.products.remove(_product);

                    this.totalValueDisplay.setText(this.totalValue());

                    grid.getDataProvider().refreshAll();

                    this.DDeleteProduct.close();
                });
                delete.getElement().setAttribute("aria-label", "Deletar Produto");

                Button update = new Button("Atualizar", _event -> {
                    this.DUpdateProduct.open();
                });
                update.getElement().setAttribute("aria-label", "Atualizar Produto");

                Button close = new Button("Fechar Tela", _event -> {
                    this.DDeleteProduct.close();
                });
                delete.getElement().setAttribute("aria-label", "Deletar Produto");

                columnLayout.add(delete);
                columnLayout.add(update);
                columnLayout.add(close);

                Dialog product = new Dialog();

                product.setWidth("400px");
                product.add(columnLayout);

                this.DDeleteProduct = product;

                this.DDeleteProduct.open();
            });

            /* settings components */

            this.dts = new Details("Pedido", new Text("foo"));
            this.dts.getElement().getStyle().set("width", "100%");
            this.dts.addThemeVariants(DetailsVariant.REVERSE, DetailsVariant.FILLED);
            this.totalValueDisplay = new Text("0.0");

            /* add components */
            content.add(grid);

            crud.add(newOrder);
            crud.add(newProduct);
            crud.add(totalValueDisplay);

            header.add(titleContainer);
            header.add(crud);

            settings.add(dts);

            main.add(content, settings);

            /* reset css */

            setSizeFull();
            getElement().getStyle().set("padding", "0");
            getElement().getStyle().set("margin", "0");
            getElement().getStyle().set("outline", "0");

            /* add components in principal component */
            add(
                header,
                main
            );
      }
}
