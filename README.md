<!-- Logo -->
<img src=".gitlab/logo.png" align="right" width="129"/>

<!-- Title -->

# Software Engineering

![ci-badge] ![license-badge] ![java-badge] ![maven-badge] ![docker-badge]

<!-- Short Description -->

> Software engineering study with CI/CD and Docker

<!-- Description -->

This project was developed as a practical application of the subject of software engineering from the information systems course.

<!-- Disclaimer -->

> :warning: **DO NOT USE IN PRODUCTION!**

---

## Project structure

```
   .
   ├── .github                     # Repository stuff.
   ├── spring                      # Spring files, config and related.
   |   ├── conf.d                  # Dynamic config files (auto_envsubst).
   |   |   └── templates           # Config template files (auto_envsubst).
   |   ├── docker-entrypoint.d     # Docker entrypoint scripts (autoexec).
   |   |   └── 00-envsubst-...     # Runs envsubst on all fiels of conf.d.
   |   ├── docker-entrypoint.sh    # Docker entrypoint script (autoexec).
   |   └── Dockerfile              # Backend Dockerfile.
   ├── database                    # Database files config and related.
   |   ├── data                    # Database files (docker_volume).
   |   └── docker-entrypoint-...   # Scripts executed on initdb (first_start).
   |       └── database.sql        # Database initial state.
   ├── nginx                       # Nginx config and related.
   |   ├── html                    # Fallback pages (docker_volume).
   |   |   ├── index.html          # Index fallback page.
   |   |   ├── 50x.html            # Error 50x fallback page.
   |   |   └── 404.html            # Error 404 fallback page.
   |   ├── conf.d                  # Dynamic config files (auto_envsubst).
   |   |   └── templates           # Config template files (auto_envsubst).
   |   ├── docker-entrypoint.d     # Docker entrypoint scripts (autoexec).
   |   |   └── 00-envsubst-...     # Runs envsubst on all fiels of conf.d.
   |   ├── docker-entrypoint.sh    # Docker entrypoint script (autoexec).
   |   └── nginx.conf              # Nginx main conf file (docker_volume)
   ├── .env.template               # ENV variables template.
   └── docker-compose.yml          # Docker Compose file.
```

## Build Instructions `[DOCKER COMPOSE]`

```sh
    # Clone the repo
    git clone https://gitlab.com/thiago-rezende/software-engineering
    cd software-engineering

    # Setup ENV file and edit if necessary
    cp .env.template .env

    # Build the docker images
    docker compose build

    # Run the database container first (first setup takes longer)
    docker compose up -d database

    # Run the spring container second (first setup takes longer)
    docker compose up -d spring

    # Run the other containers
    docker compose up -d
```

## Live Reload `[VAADIN]`

```sh
    # Open shell inside the spring container
    docker-compose exec spring /bin/sh

    # Go to the application dir
    cd app

    # Compile (repeat at every change)
    mvn compile
```

<!-- Links -->

[openjdk-url]: https://openjdk.java.net
[maven-url]: https://maven.apache.org
[docker-url]: https://www.docker.com

<!-- Badges -->

[ci-badge]: https://img.shields.io/gitlab/pipeline/thiago-rezende/software-engineering/main?style=flat-square
[license-badge]: https://img.shields.io/badge/license-MIT-blue.svg?style=flat-square
[java-badge]: https://img.shields.io/badge/Java-11-blueviolet.svg?style=flat-square
[maven-badge]: https://img.shields.io/badge/Maven-3-orange.svg?style=flat-square
[docker-badge]: https://img.shields.io/badge/Docker-20.10-darkblue.svg?style=flat-square
